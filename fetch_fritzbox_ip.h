#ifndef FETCH_FRITZBOX_IP_H
#define FETCH_FRITZBOX_IP_H

#include <string>

std::string fetch_fritzbox_ip(std::string fritzbox_host, bool &ok);

#endif // FETCH_FRITZBOX_IP_H
