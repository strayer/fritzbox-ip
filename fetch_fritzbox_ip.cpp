#include "fetch_fritzbox_ip.h"

#include <stdio.h>
#include <iostream>
#include <regex>
#include <curl/curl.h>

static size_t fetch_fritzbox_ip_curl_write_callback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

std::string fetch_fritzbox_ip(std::string fritzbox_host, bool &ok)
{
    CURL *curl;
    CURLcode res;

    curl = curl_easy_init();

    ok = false;

    if (curl)
    {
        std::string url = "http://" + fritzbox_host + ":49000/upnp/control/WANIPConn1";
        std::string curl_response;

        curl_easy_setopt(curl, CURLOPT_URL, url.data());
        curl_easy_setopt(curl, CURLOPT_POST, 1);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "<?xml version='1.0' encoding='utf-8'?> <s:Envelope s:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/' xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'> <s:Body> <u:GetExternalIPAddress xmlns:u=\"urn:schemas-upnp-org:service:WANIPConnection:1\" /> </s:Body> </s:Envelope>");

        struct curl_slist *chunk = NULL;
        chunk = curl_slist_append(chunk, "Content-Type: text/xml; charset=\"utf-8\"");
        chunk = curl_slist_append(chunk, "SoapAction:urn:schemas-upnp-org:service:WANIPConnection:1#GetExternalIPAddress");

        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fetch_fritzbox_ip_curl_write_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &curl_response);
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 5);

        res = curl_easy_perform(curl);

        if(res != CURLE_OK)
            return curl_easy_strerror(res);
        else
        {
            std::regex rgx("([0-9]+(?:\\.[0-9]+){3})");
            std::smatch match;

            if (std::regex_search(curl_response, match, rgx))
            {
                ok = true;
                return match[1];
            }
            else
                return curl_response;
        }

        curl_easy_cleanup(curl);
    }

    return std::string();
}
