#include <stdio.h>
#include <string>
#include <tclap/CmdLine.h>
#include "fetch_fritzbox_ip.h"

int main(int argc, char** argv)
{
    try {
        TCLAP::CmdLine cmd("This command tries to retrieve the external IP address of a FritzBox via its UPnP API", ' ', "1.0");

        TCLAP::ValueArg<std::string> hostArg("", "fritzbox-host", "Hostname/IP of your FritzBox, defaults to fritz.box", false, "fritz.box", "string");
        cmd.add(hostArg);

        cmd.parse(argc, argv);

        std::string fritzbox_host = hostArg.getValue();

        bool ok;
        std::string ip = fetch_fritzbox_ip(fritzbox_host, ok);

        if (!ok)
        {
            std::cerr << "Error: " << ip << std::endl;
            return -1;
        }
        else
        {
            std::cout << ip << std::endl;
        }
    }
    catch (TCLAP::ArgException &e)
    {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    }

    return 0;
}
